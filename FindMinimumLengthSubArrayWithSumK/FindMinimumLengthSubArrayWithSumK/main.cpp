#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        int         requiredSum;
    
        void printMinimumLengthSubarrayWithRequiredSum(int start , int len)
        {
            for(int i = start ; i < start + len ; i++)
            {
                cout<<arrVector[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> a , int rS)
        {
            arrVector   = a;
            requiredSum = rS;
        }
    
        void findAndPrintMinimumLengthSubarrayWithRequiredSum()
        {
            int len    = (int)arrVector.size();
            int start  = -1;
            int maxLen = INT_MAX;
            for(int i = 0 ; i < len-1 ; i++)
            {
                int sum = arrVector[i];
                for(int j = i+1 ; j < len ; j++)
                {
                    if(maxLen > j - i + 1)
                    {
                        sum += arrVector[j];
                        if(sum == requiredSum)
                        {
                            start  = i;
                            maxLen = j - i + 1;
                        }
                    }
                }
            }
            if(start == -1)
            {
                cout<<"No Subarry found"<<endl;
            }
            else
            {
                printMinimumLengthSubarrayWithRequiredSum(start , maxLen);
            }
        }
};

int main(int argc, const char * argv[])
{
    //int         arr[] = {2,3,1,2,4,3};
    int         arr[] = {1, 4, 20, 3, 10, 5};
    vector<int> arrVector;
    int         len   = sizeof(arr)/sizeof(arr[0]);
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine(arrVector , 33);
    e.findAndPrintMinimumLengthSubarrayWithRequiredSum();
    return 0;
}
